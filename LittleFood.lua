LittleFood = {}
LittleFood.name = "LittleFood"

LittleFood.buffs = {
    "increase max health",
    "increase max stamina",
    "increase max magicka",
    "increase max health & magicka",
    "increase max health & stamina",
    "increase max magicka & stamina",
    "increase max health, magicka & stamina",
    "health recovery",
    "stamina recovery",
    "magicka recovery",
    "health & magicka recovery",
    "health & stamina recovery",
    "magicka & stamina recovery",
    "health, magicka & stamina recovery"
}

function LittleFood:GetConsumableStatus()
    local buffCount = GetNumBuffs("player")
    local hasBuff = false

    for i = 1, buffCount do
        local buffItem = GetUnitBuffInfo("player", i)

        for _,v in pairs(LittleFood.buffs) do
            if v == buffItem:lower() then
                hasBuff = true
                break
            end
        end
    end

    if not hasBuff then
        ZO_Alert(UI_ALERT_CATEGORY_ALERT, SOUNDS.DEFAULT_CLICK, "You feel hungry and parched.")
    end
end

function LittleFood:Initialize()

    EVENT_MANAGER:RegisterForUpdate("foodUpdate", 10000, function()
        LittleFood:GetConsumableStatus()
    end)

end

function LittleFood.OnAddOnLoad(event, addonName)
    if addonName == LittleFood.name then
        LittleFood:Initialize()
    end
end

EVENT_MANAGER:RegisterForEvent(LittleFood.name, EVENT_ADD_ON_LOADED, LittleFood.OnAddOnLoad)